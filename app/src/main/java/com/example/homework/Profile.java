package com.example.homework;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Profile extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Button btnDatePicker = findViewById(R.id.date_pick);

        Button btnSave;
        btnSave = findViewById(R.id.btn_save);

        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialg();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String format = "MM/dd/yy";

        SimpleDateFormat datePick = new SimpleDateFormat(format, Locale.US);
        TextView textView = findViewById(R.id.date_text);
        textView.setText(datePick.format(calendar.getTime()));


    }

    public void openDialg() {

        RadioGroup radioGroup;
        RadioButton radioButton;

        Dialog dialog = new Dialog(Profile.this);
        dialog.setContentView(R.layout.activity_dialog);
        TextView nameText, jobText, genderText, birthText;
        nameText = dialog.findViewById(R.id.txt_name);
        jobText = dialog.findViewById(R.id.txt_job);
        genderText = dialog.findViewById(R.id.txt_gender);
        birthText = dialog.findViewById(R.id.txt_birth_date);

        EditText n = findViewById(R.id.edit_name);
        String name = n.getText().toString();

        radioGroup = (RadioGroup) findViewById(R.id.gender_radio);
        int isSelected = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(isSelected);
        nameText.setText(name);
        String g = "Gender:   " + radioButton.getText().toString();
        genderText.setText(g);

        TextView d = findViewById(R.id.date_text);
        String date = "Date of Birth:   " + d.getText().toString();
        birthText.setText(date);

        Spinner mySpinner = (Spinner) findViewById(R.id.spin_job);
        String j = "Occupation:   " + mySpinner.getSelectedItem().toString();
        jobText.setText(j);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        dialog.getWindow().setAttributes(lp);
        dialog.show();


    }

}
