package com.example.homework;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.ResultSet;

public class CreateNote extends AppCompatActivity implements View.OnClickListener {
    EditText title, des;
    String tilteText, desText;
    Button cancel, save;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        title = findViewById(R.id.title_edit_text);
        des = findViewById(R.id.des_edit_text);
        cancel = findViewById(R.id.btn_cancel);
        save = findViewById(R.id.btn_save);


        cancel.setOnClickListener(this);
        save.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        if (v == save) {
            Intent intentNote = new Intent();

            tilteText = title.getText().toString();
            desText = des.getText().toString();

            if (tilteText.equals("") || desText.equals("")) {
                Toast.makeText(this, "Invalid Input", Toast.LENGTH_LONG).show();
            } else {

                Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
                intentNote.putExtra("TITLE", tilteText);
                intentNote.putExtra("DES", desText);
                setResult(Activity.RESULT_OK, intentNote);
                finish();
            }


        }
        if (v == cancel) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        }


    }
}
