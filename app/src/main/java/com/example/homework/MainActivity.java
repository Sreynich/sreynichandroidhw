package com.example.homework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FrameLayout contact, call, profile, gallery, note;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contact = findViewById(R.id.contact);
        call = findViewById(R.id.call);
        profile = findViewById(R.id.profile);
        gallery = findViewById(R.id.gallery);
        note = findViewById(R.id.note);

        contact.setOnClickListener(this);
        call.setOnClickListener(this);
        profile.setOnClickListener(this);
        note.setOnClickListener(this);
        gallery.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == contact) {

            Intent intent = new Intent(getApplicationContext(), Contact.class);
            startActivity(intent);

        }
        if (v == profile) {
            Intent intent = new Intent(getApplicationContext(), Profile.class);
            startActivity(intent);
        }
        if (v == gallery) {

            Intent intent = new Intent(getApplicationContext(), Gallery.class);
            startActivity(intent);

        }
        if (v == note) {

            Intent intent = new Intent(getApplicationContext(), Note.class);
            startActivity(intent);
        }
        if (v == call) {

            Intent intent = new Intent(getApplicationContext(), Call.class);
            startActivity(intent);

        }
    }
}
