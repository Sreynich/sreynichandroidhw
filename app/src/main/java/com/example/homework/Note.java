package com.example.homework;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Note extends AppCompatActivity {


    GridLayout gridLayout;
    CardView noteCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        noteCard = findViewById(R.id.note_card);

        noteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addIntent = new Intent(getApplicationContext(), CreateNote.class);
                startActivityForResult(addIntent, 1000);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String title = data.getStringExtra("TITLE");
                String des = data.getStringExtra("DES");

                gridLayout = findViewById(R.id.note_grid_layout);

                CardView cardView = new CardView(getApplicationContext());
                GridLayout.LayoutParams param = new GridLayout.LayoutParams();
                param.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
                param.width = 200;
                param.height = convertDpToPx(150);

                cardView.setLayoutParams(param);
                cardView.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                cardView.setPadding(5, 5, 5, 5);


                LinearLayout linearLayout = new LinearLayout(getApplicationContext());

                linearLayout.setOrientation(LinearLayout.VERTICAL);

                linearLayout.setPadding(50, 50, 50, 50);

                TextView titleText = new TextView(this);
                TextView desText = new TextView(this);


                titleText.setTextSize(22);
                titleText.setTypeface(Typeface.DEFAULT_BOLD);
                titleText.setTextColor(Color.BLACK);
                titleText.setMaxLines(1);
                titleText.setEllipsize(TextUtils.TruncateAt.END);

                desText.setTextSize(18);
                desText.setMaxLines(2);
                desText.setEllipsize(TextUtils.TruncateAt.END);
                desText.setTextColor(Color.BLACK);
                titleText.setText(title);
                desText.setText(des);

                linearLayout.addView(titleText);
                linearLayout.addView(desText);
                cardView.addView(linearLayout);

                gridLayout.addView(cardView);

            }
        }

    }

    public int convertDpToPx(int dp) {
        float density = getApplicationContext().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }


}
